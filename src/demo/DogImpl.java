package demo;

import java.util.LinkedList;
import java.util.List;

public class DogImpl implements Dog {

	// This is a normal field
	private String name;
	
	// We declare a static string and give it a value
	static String noise = "woof";
	
	// We cannot assign values when we declare this type of variable
	public static List<String> enjoysActivities = new LinkedList<String>();
	
	// we can use a static block to set values to static variables
	static {
		enjoysActivities.add("Eating");
		enjoysActivities.add("Sleeping");
		enjoysActivities.add("Barking");
    }
	
	DogImpl(String myName) {
		name = myName;		
	}
	
	// static and non-static methods both have access to static fields
	static void makeNoise() {
		System.out.println(noise);
	}
	
	// a non-static method has access to non-static fields (and static fields)
	public void nonStaticMethod() {
		System.out.println(name + " says " + DogImpl.noise);		
	}
	
	// a static method does not have access to non-static fields
	static public void staticMethod() {
		// This next line does not work!
		// System.out.println(name);
	}
		
}
