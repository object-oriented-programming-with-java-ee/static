package demo;

public class Singleton {

	private Singleton() {}
    
	// this is called a static nested class
    private static class SingletonHolder {    
        public static final Singleton instance = new Singleton();
    }    
    
    // this is called an inner class
    private class InnerClass {
    	
    }
 
    public static Singleton getInstance() {    
        return SingletonHolder.instance;    
    }    
}
