package demo;

public interface Dog {

	static Boolean isFriendly = false;
	
	// Classes that implement this interface will not have this method
	static void greetPostman() {
		if (isFriendly) {
			System.out.println("The dog licks the postmans hand");
		} else {
			System.out.println("The dog growls at the postman");
		}
	}
	
	// Default methods are made available in the implementing class
	default void sleep() {
		System.out.println("The dog sleeps");
	}
	
}
