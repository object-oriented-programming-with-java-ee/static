package demo;

public class Main {

	public static void main(String[] args) {		
		
		// ---------------------------------------------------------------------
		
		// Can call a static method without an object being instantiated
		DogImpl.makeNoise();
				
		// ---------------------------------------------------------------------
		
		// Create two objects from the same class
		DogImpl barksley = new DogImpl("Barksley");
		
		DogImpl sirbarksalot = new DogImpl("Sir Barksalot");
		
		// Both objects output the same message ("woof"); 
		barksley.nonStaticMethod();
		
		sirbarksalot.nonStaticMethod();
		
		// ---------------------------------------------------------------------
		
		// Changing the static variable for one object changes it for all
		barksley.noise = "bark bark";
		
		// barksley makes the new noise "bark bark"
		barksley.nonStaticMethod();
		
		// sirbarksalot also goes "bark bark" because the static variable
		// is shared between all instances of the class
		sirbarksalot.nonStaticMethod();
		
		// ---------------------------------------------------------------------
		
		// You can define static members on interfaces
		Dog.greetPostman();
		
		// ---------------------------------------------------------------------
		
		// Static fields and methods in interfaces are final
		// This next line does not work
		// Dog.isFriendly = true;
		
		// ---------------------------------------------------------------------
		
		// Default methods are made available in the implementing class
		
		// this works because the method is default
		barksley.sleep();
		
		// Unless the method is default, the implementing class does not have it
		
		// this next line does not work
		// barksley.greetPostman();
						
	}

}
